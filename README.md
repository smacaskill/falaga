This code was built using Visual Studio 2017, Windows Forms App, using the C# language.

You can copy this code into a new Windows Form App, provided that you have the proper form built with all properly named controls and buttons. As this would be very hard to accomplish, consider renaming appropriate methods to whichever button names you like.

NOTE: The license agreement was selected due to its open source nature, where copyright is maintained by the developer(me) but allows others to copy or modify the code as they see fit for their own distribution. This allows me to also continue to freely use and distribute what I have built and can do as I please with my own copies or modifications in the future.